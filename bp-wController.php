<?php

    class wController
    {
    
        public function __construct()
        {
        } 

        public function index()
        {
            require_once "bp-content/views/index.php";
        }    

        public function p404()
        {
            require_once "bp-content/views/404.php";
        }

        public function boutique()
        {
            require_once "bp-content/views/store.php";
        }

        public function connexion()
        {
            require_once "bp-content/views/register.php";
        }

        public function resetPassword()
        {
            require_once "bp-content/views/fPassword.php";
        }

        public function newPassword()
        {
            require_once "bp-content/views/nPassword.php";
        }

        public function eAdmin()
        {
            // require_once "bp-admin/mycrud/mycrud.php";
            header("Location:myDigiPartner/");
        }

    }


?>