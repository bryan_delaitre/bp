<?php
    session_start();
    require_once "bp-config.php";
    require_once "bp-wController.php"; // wController = Welcome Controller

    require_once "bp-content/classes/UserManager.php"; // on charge la classe utilisateur

    require_once "bp-login.php";
    require_once "bp-signup.php";

    if(isset($_POST["deconnexion"])&&$_POST["deconnexion"]==="deconnexion")
    {

            $user_Manager = new UserManager();
            $user_Manager -> deconnexionUser();

    }

    $user = new User();

    try{

        if (empty($_GET['page']))
        {
            $controller = new wController();
            $controller->index();
        }
        else
        {

            $url = explode("/", filter_var($_GET['page'], FILTER_SANITIZE_URL));
            switch ($url[0])
            {
                case "boutique":
                    $controller = new wController();
                    $controller->boutique();
                    break;

                case "mots-de-passe-oublie":
                    $controller = new wController();
                    $controller->resetPassword();
                    break;

                case 'reinitialision-mot-de-passe':
                    if(count($url)>=4)
                    {
                        $_SESSION['reset_id']=$url[2];
                        $_SESSION['reset_token']=$url[3];
                        header('Location:/bp/nouveau-mot-de-passe');
                    }
                    else
                    {
                        $controller = new wController();
                        $controller -> p404();
                    }
                    break;

                case 'nouveau-mot-de-passe':
                    $controller = new wController();
                    $controller->newPassword();
                    break;

                case 'eAdmin':
                    if(isset($_SESSION['session_Admin']))
                    {
                        if($_SESSION['session_Admin']==='1')
                        {
                            $controller = new wController();
                            $controller -> eAdmin();
                        }
                    }
                    else
                    {
                        header('Location:/bp');
                    }
                    break;
            }

        }

    }  
    catch (Exception $e) 
    {
        echo $e->getMessage();
    }

?>