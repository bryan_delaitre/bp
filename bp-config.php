<?php
    require_once "bp-content/classes/Medoo.php";

    use Medoo\Medoo;

    $servername='localhost';
    $username='root';
    $dbname='bateau_pirate_bdd';

    try{

        // Connexion au serveur SQL
        $sqlconfig = new PDO("mysql:host=$servername;charset=utf8",$username);
        $sqlconfig -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // Création de la BDD en local vu que je change régulièrement de pc ou pour le correcteur
        $sql = "CREATE DATABASE IF NOT EXISTS bateau_pirate_bdd DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci";
        $sqlconfig -> exec($sql);

        // Connexion à la base de donnée avec la classe du framework Medoo
        $database = new Medoo ([
            'type' => 'mysql',
            'host' => 'localhost',
            'database' => 'bateau_pirate_bdd',
            'username' => 'root',
            'password' => '',

            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_general_ci',
        ]);

        // // Connexion à la BDD une fois créée ou déjà éxistante  ////// (à enlever) \\\\\\
        // $sqlconfig = new PDO("mysql:host=$servername;dbname=$dbname;charset=utf8",$username);
        // $sqlconfig -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        // Création de la table "Connexions" pour enregistrer toutes les tentatives de connexions sur le site web.
        $database -> create("users_connexions",[
            'connexion_Id' => [
                'BIGINT',
                "UNSIGNED",
                "NOT NULL",
                "AUTO_INCREMENT",
                "PRIMARY KEY"
            ],
            'connexion_Login' => [
                'VARCHAR(80)',
                'NOT NULL'
            ],
            'connexion_Datetime' => [
                'TIMESTAMP',
                'NOT NULL',
                'default',
                'CURRENT_TIMESTAMP'
            ],
            'connexion_Ip' => [
                'VARCHAR(20)',
                'NOT NULL'
            ]
        ],[
            "ENGINE" => "MyISAM"
        ]);


        // Création de la table "Users" afin de pouvoir enregistrer tous les utilisateurs inscrits sur le site.        
        $database -> create('Users',[
            'user_Id' => [
                'BIGINT',
                'UNSIGNED',
                'NOT NULL',
                'AUTO_INCREMENT',
                'PRIMARY KEY'
            ],
            'user_Lastname' => [
                'VARCHAR(80)',
                'NOT NULL'
            ],
            'user_Firstname' => [
                'VARCHAR(80)',
                'NOT NULL'
            ],
            "user_Is_Admin" => [
                'TINYINT(1)',
                'NOT NULL',
                'default',
                '0'
            ],
            'user_Email' => [
                'VARCHAR(80)',
                'NOT NULL',
                'UNIQUE'
            ],
            'user_Password' => [
                'VARCHAR(70)',
                'NOT NULL'
            ],
            'user_Type' => [
                'VARCHAR(13)',
                'NOT NULL'
            ],
            'user_Verified' => [
                'TINYINT(1)',
                'NOT NULL',
                'default',
                '0'
            ],
            'user_Is_Blacklisted' => [
                'TINYINT',
                'NOT NULL',
                'default',
                '0'
            ]
            
        ],[
            "ENGINE" => "MyISAM"
        ]);

        // Création d'une table d'adress IP blacklisté
        $database->create('Blacklist',
        [
            'blacklisted_Id' => [
                'BIGINT',
                'UNSIGNED',
                'NOT NULL',
                'AUTO_INCREMENT',
                'PRIMARY KEY'
            ],
            'ip' => [
                'VARCHAR(70)',
                'NOT NULL'
            ],
            'already_Visited' => [
                'TINYINT(1)',
                'UNSIGNED',
                'NOT NULL'
            ],
            'due_Of_Bad_Action' => [
                'TINYINT(1)',
                'UNSIGNED',
                'NOT NULL'
            ],
            'linked_User_Id' => [
                'BIGINT',
                'UNSIGNED',
                'NOT NULL',
                'DEFAULT',
                '0'
            ]
        ]);

        // Création d'une table d'utilisateur supprimé
        $database->create('Users_Deleted',[
            'deleted_Id' => [
                'BIGINT',
                'UNSIGNED',
                'NOT NULL',
                'AUTO_INCREMENT',
                'PRIMARY KEY'
            ],
            'deleted_user_Id' => [
                'BIGINT',
                'UNSIGNED',
                'NOT NULL',
                'DEFAULT',
                '0'
            ],
            'deleted_Datetime' => [
                'TIMESTAMP',
                'NOT NULL',
                'default',
                'CURRENT_TIMESTAMP'
            ],
            'still_deleted' => [
                'TINYINT(1)',
                'NOT NULL',
                'default',
                '1'
            ]

        ]);

        
        // Création de la table "Users_Pass_Reset" afin de nous permettre la réinitialisation du mots passe d'un utilisateur en stockant un token en base de donnée.
        $database->create("Users_Pass_Reset",[
            'Pass_Reset_Id' => [
                'BIGINT',
                'UNSIGNED',
                'NOT NULL',
                'AUTO_INCREMENT',
                'PRIMARY KEY'
            ],
            'user_Id' => [
                'BIGINT',
                'UNSIGNED',
                'NOT NULL'
            ],
            'Pass_Reset_Token' => [
                'VARCHAR(70)',
                'NOT NULL',
                'UNIQUE'
            ],
            'Pass_Reset_Datetime' => [
                'TIMESTAMP',
                'NOT NULL',
                'default',
                'CURRENT_TIMESTAMP'
            ],
            'Pass_Reset_Expired' => [
                'TINYINT(1)',
                'NOT NULL',
                'default',
                '0'
            ]
        ],[
            "ENGINE" => "MyISAM"
        ]);

        // Création d'une table de logs
        $database->create('users_logs',[
            'user_Id' => [
                'BIGINT',
                'UNSIGNED',
                'NOT NULL'
            ],  
            'log_Event' => [
                'VARCHAR(200)',
                'NOT NULL'
            ],
            'log_Event_Type' => [
                'VARCHAR(20)',
                'NOT NULL'
            ],
            'log_Event_Datetime' => [
                'TIMESTAMP',
                'NOT NULL',
                'default',
                'CURRENT_TIMESTAMP'
            ],
            'log_Event_Report' => [
                'VARCHAR(100)',
                'NOT NULL'
            ],
            'log_Id' => [
                'BIGINT',
                'UNSIGNED',
                'NOT NULL',
                'AUTO_INCREMENT',
                'PRIMARY KEY'
            ]
            
        ],[
            "ENGINE" => "MyISAM"
        ]);

        $database -> create ('tokens',
        [
            'token_Id' => [
                'BIGINT',
                'UNSIGNED',
                'NOT NULL',
                'AUTO_INCREMENT',
                'PRIMARY KEY'
            ],
            'type' => [
                'VARCHAR(7)',
                'NOT NULL'
            ],
            'user_Id' => [
                'BIGINT',
                'UNSIGNED',
                'NOT NULL'
            ],
            'token' => [
                'VARCHAR(70)',
                'NOT NULL',
                'UNIQUE'
            ],
            'token_Datetime' => [
                'TIMESTAMP',
                'NOT NULL',
                'default',
                'CURRENT_TIMESTAMP'
            ],
            'token_Expired' => [
                'TINYINT(1)',
                'NOT NULL',
                'default',
                '0'
            ]      
        ]);

    }catch(PDOException $e){
        echo "Erreur :" . $e->getMessage();
    }

    // Enfin, on vient forcer la création d'un utilisateur "Admin", même si celui ci vient à être supprimé par inatention dans le CRUD, il sera recréer aussi tôt dès le chargement du index.php ou mycrud.php
    $adminVerif=$database->select('users','*',[
        'user_Email' => 'admin@localhost.bp'
    ]); 

    if(empty($adminVerif[0])){
        $database -> insert('users',[
            'user_Lastname' => 'ADMIN',
            "user_Firstname" => 'Demo',
            "user_Email" => "admin@localhost.bp",
            "user_Password" => password_hash("adminCRUD",PASSWORD_BCRYPT),
            "user_Type" => "DEMO ADMIN",
            "user_Is_Admin" => 1
        ]);
    }

?>