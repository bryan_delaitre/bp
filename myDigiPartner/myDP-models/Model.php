<?php

    require 'Medoo.php';

    abstract class Model{

        private const HOST = 'localhost';
        private const DB = 'bateau_pirate_bdd';
        private const USER = 'root';
        private const PWD = '';

        private static $database;

        private static function initDatabase(){

            self::$database = new Medoo\Medoo([
                'database_type' => 'mysql',
                'database_name' => self::DB,
                'server' => self::HOST,
                'username' => self::USER,
                'password' => self::PWD,
                "charset" => "utf8",
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            ]);

        }

        protected function getDatabase()
        {

            if (self::$database === null) {
                self::initDatabase();
            }

            return self::$database;

        }

    }

?>