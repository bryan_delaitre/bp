<?php
    
    require_once 'myDP-models/Model.php';
    require_once 'myDP-models/admin.php';
    require_once 'library/safetyFirst.php';
    
    class AdminManager extends Model {

        private $servername='localhost';
        private $username='root';
        private $dbname='bateau_pirate_bdd';
        private $sqlconfig;
        public $user;

        public function __construct(){

            
            // $user = $this -> getDatabase() -> select('users','*',[
            //     'user_Id' => $var
            // ]);

            // $this -> user = new User($user);


        }

        public function getUserList($user=null){

            // $getUsers=  $this -> getDatabase() -> prepare("SELECT * FROM Users ORDER BY user_Id");
            // $getUsers->setFetchMode(PDO::FETCH_ASSOC);
            // $getUsers->execute(array());
            // $db_all_Users = $getUsers->fetchAll();
            
            $users = $this -> getDatabase() -> select('users','*');
            return $users;


        }

        public function getUserById($var){
            // $getUser=  $this -> getDatabase() ->prepare("SELECT * FROM Users WHERE user_Id = ?");
            // $getUser->setFetchMode(PDO::FETCH_ASSOC);
            // $getUser->execute(array(
            //     $user_Id
            // ));
            // $User = $getUser->fetchAll();

            $user = $this -> getDatabase() -> select('users','*',[
                'user_Id' => $var
            ]);

            return $user;
        }

        public function deleteUserById($var){
            // $deleteUser=  $this -> getDatabase() ->prepare("DELETE FROM Users WHERE user_Id = ?");
            // $deleteUser->execute(array(
            //     $user_Id
            // ));

            $this -> getDatabase() -> delete('users',[
                'user_Id' => $var
            ]);

        }

        // $user_Id,$user_Lastname,$user_Firstname,$user_Email,$user_Type

        public function updateUser(){

            if(isset($_POST['id'])){$id=iProtectMySQL($_POST['id']);}
            if(isset($_POST['firstname'])){$firstname=iProtectMySQL($_POST['firstname']);}
            if(isset($_POST['lastname'])){$lastname=iProtectMySQL($_POST['lastname']);}
            if(isset($_POST['email'])){$email=safeEmail($_POST['email']);}
            if(isset($_POST['type'])){$type=iProtectMySQL($_POST['type']);}

            if(isset($_POST['update']))
            {
                if($_POST['update']==='Modifier')
                {

                    if(isset($type))
                    {

                        $this -> getDatabase() -> update ('users',[
                            'user_Lastname' => $lastname,
                            'user_Firstname' => $firstname,
                            'user_Email' => $email,
                            'user_Type' => $type
                        ],[
                            'user_Id' => $id
                        ]);

                    } else {

                        $this -> getDatabase() -> update ('users',[
                            'user_Lastname' => $lastname,
                            'user_Firstname' => $firstname,
                            'user_Email' => $email
                        ],[
                            'user_Id' => $id
                        ]);

                    }

                    header('Location:http://localhost/bp/myDigiPartner/user/'.$id);

                }
            }



        }

        public function getLogs(){
            $logs = $this -> getDatabase() -> select ('users_logs','*');
            
            return $logs;

            
        }

    }   
?>