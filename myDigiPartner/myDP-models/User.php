<?php

    class User extends Model {
        private $user_Id;
        private $user_Email;
        private $user_Fullname;
        private $user_Firstname;
        private $user_Lastname;
        private $user_Password;
        private $user_Type;
        private $user_Verify;
        private $user_Blacklisted;

        public function __construct($id=null, $email=null, $userFirstname=null, $userLastname=null, $password=null, $type=null){  
           
            $this -> user_Id = $var['user_Id'];
            $this -> user_Email = $var['user_Email'];
            $this -> user_Fullname = $var['user_Firstname'].' '.$var['user_Lastname'];
            $this -> user_Firstname = $var['user_Firstname'];
            $this -> user_Lastname = $var['user_Lastname'];
            $this -> user_Password = $var['user_Password'];
            $this -> user_Type = $var['user_Type'];
            $this -> user_Verify = $var['user_Verified'];
            $this -> user_Blacklisted = $var['user_Is_Blacklisted']; 

            // $this -> user_Id = $id;
            // $this -> user_Email = $email;
            // $this -> user_Fullname = strtoupper($userLastname)." ".ucfirst($userFirstname);
            // $this -> user_Firstname = $userFirstname; 
            // $this -> user_Lastname = $userLastname;
            // $this -> user_Password = $password;
            // $this -> user_Type = $type;
        
        }

        public function getId(){
            return $this -> user_Id;
        }

        public function setId($var){
            $this -> user_Id = $var;
        }

        public function getEmail(){
            return $this -> user_Email;
        }

        public function setEmail($var){
            $this -> user_Email = $var;
        }

        public function getFullname(){
            return $this -> user_Fullname;
        }

        public function setFullname($var){
            $this -> user_Fullname = $var;
        }

        public function getFirstname(){
            return $this -> user_Firstname;
        }

        public function setFirstname($var){
            $this -> $user_Firstname = $var;
        }

        public function getLastname(){
            return $this -> $user_Lastname;
        }

        public function setLastname($var){
            $this -> user_Lastname = $var;
        }

        private static function getPassword(){
            return $this -> user_Password;
        }

        private static function setPassword($var){
            $this -> user_Password = $var;
        }

        public function getType()
        {
            return $this -> user_Type;
        }

        public function getType_()
        {
            if($this -> user_Type ==='1')
            {
                return "Particulier";
            }
            else if($this -> user_Type ==='0')
            {
                return "Professionnel";
            }
            else if($this -> user_Type ==='DEMO ADMIN' || $this -> user_Type ==='ADMIN')
            {
                return 'Administrateur';
            }
        }

        public function setType($var)
        {
            $this -> user_Type = $var;
        }

        public function getUserVerify()
        {
            return $this -> user_Verify;
        }

        public function getUserVeirfy_()
        {
            if($this -> user_Verify === '1')
            {
                return "Oui";
            }
            else
            {
                return "Non";
            }
        }

        public function setUserVerify($var)
        {
            $this -> user_Verify = $var;
        }

        


       
    }




?>