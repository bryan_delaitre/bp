<?php

    session_start();

    require_once "config.php";
    // require_once "../bp-config.php";
    require_once "myDP-Controller.php";

    require_once "myDP-models/adminManager.php";
    $admin_Manager = new AdminManager(); 

    include "myDP-views/header.php";
    include "myDP-views/head.php";

    if(isset($_POST["deconnexion"])&&$_POST["deconnexion"]==="deconnexion")
    {

        $admin_Manager -> deconnexionAdmin();         

    }
         
    try
    {

        if(isset($_SESSION['session_Admin']))
        {

            if($_SESSION['session_Admin']==='1')
            {

                if(empty($_GET['page']))
                {
        
                    $controller = new myDP_Controller();
                    $controller -> index();
        
                }
                else
                {
        
                    $url = explode("/", filter_var($_GET['page'], FILTER_SANITIZE_URL));
                    switch ($url[0])
                    {
                        case 'users':
                            $controller = new myDP_Controller();
                            $controller -> users();
                            break;
                        
                        case 'user':
                            if(isset($url[1])&&$url[1]!==null)
                            {
                                if(isset($url[2]))
                                {
                                    if($url[2]==='update')
                                    {
                                        $controller = new myDP_Controller();
                                        $controller -> update_User();
                                    }
                                    else if($url[2]==='delete')
                                    {
                                        $controller = new myDP_Controller();
                                        $controller -> delete_User();
                                    }
                                }
                                else
                                {
                                    $controller = new myDP_controller();
                                    $controller -> user();
                                }            
                            }
                            else
                            {
                                header('Location:/bp/myDigiPartner');
                            }
                            break;

                        case 'logs':
                            $controller = new myDP_Controller();
                            $controller -> logs();
                            break;
                    }
                }
            } else {

                header('Location:http://localhost/bp');

            }


        }

    }
    catch(Exception $e)
    {

    }


    
    if(isset($_GET['delU'])){
        echo "<div class='crud_Popup'>",
                "<div class='crud_Popup_Content'>";
                include "../functions/deleteUser.php";
        echo    "</div>",
             "</div>";
    }
    if(isset($_GET['modU'])){
        echo "<div class='crud_Popup'>",
                "<div class='crud_Popup_Content'>";
                include "../functions/modifUser.php";
        echo    "</div>",
             "</div>";
    }

    // if(isset($_GET['modif_Dismiss'])){
    //     header('Location:mycrud.php?modif_Confirmed=false');
    // }


    // if(isset($_GET['modif_Confirm'])){
    //     header('Location:mycrud.php?modif_Confirmed=true');
    //     if($_GET['modif_Confirm']==='Modifier'){
    //         if($_GET['modif_Type']==1){
    //             $user_Type="Particulier";
    //         }else if($_GET['modif_Type']==0){
    //             $user_Type="Professionnel";
    //         }else if($_GET['modif_Type']==2){
    //             $user_Type="ADMIN";
    //         }else{
    //             $user_Type="Inconnu";
    //         }
    //         $admin -> updateUser($_GET['modif_Id'],$_GET['modif_Lastname'],$_GET['modif_Firstname'],$_GET['modif_Email'],$user_Type);
    //     }
    // }
    // if(isset($_GET['modif_Confirmed'])){
    //     if($_GET['modif_Confirmed']==="true"){
    //         echo "<div class='crud_Popup' id='modif_Dismiss'>",
    //                 "<div class='crud_Popup_Content'>",
    //                     "<h3 style='color:rgb(89, 173, 81)'>Les informations du clients on été mise à jours.</h3>",
    //                 "</div>",
    //             "</div>";
    //     }
    //     if($_GET['modif_Confirmed']==="false"){
    //         echo "<div class='crud_Popup' id='modif_Dismiss'>",
    //                 "<div class='crud_Popup_Content'>",
    //                     "<h3 style='color:rgb(189, 22, 22)'>Les modifications n'ont pas été enregistrés.</h3>",
    //                 "</div>",
    //             "</div>";
    //     }
    // }



?>


