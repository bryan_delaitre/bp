<?php

    class myDP_Controller
    {

        public function __construct()
        {

        }

        public function index()
        {

            require_once "myDP-views/user-list.php";

        }

        public function users()
        {
            require_once "myDP-views/user-list.php";
        }

        public function user()
        {
            require_once "myDP-views/user.php";
        }

        public function logs()
        {
            require_once "myDP-views/logs.php";
        }

        public function update_User()
        {
            $admin_Manager = new AdminManager();
            require_once "myDP-views/loader.php";
            $admin_Manager -> updateUser();
        }

    }

?>
