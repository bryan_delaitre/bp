<!DOCTYPE html>
<html lang="fr-FR">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo URL_SITE; ?>myDP-public/css/crud.css">
    <script type="text/javascript" src="<?php echo URL_SITE; ?>myDP-public/js/jquery-3.6.0.min.js"></script>
    <script type="text/javascript" src="<?php echo URL_SITE; ?>myDP-public/js/crud.js"></script>
    <title>Panneau de contrôle</title>
</head>