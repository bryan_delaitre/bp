<header id='header'>
    <img src='<?php echo URL_SITE; ?>myDP-public/svg/myDP.svg' id='logo'/>
    <nav>
        <div class='nav-head' id='users'>
            <div class='nav-icon'></div>
            <h3 class='nav-title'>UTILISATEURS</h3>
        </div>
        <ul class='nav-list'>
            <li id='list'><a href='<?php echo URL_SITE; ?>users'>Liste</a></li>
            <li id='logs'><a href='<?php echo URL_SITE; ?>logs'>Journal d'évènement</a></li>
        </ul>
        <div class='nav-head' id='shop'>
            <div class='nav-icon'></div>
            <h3 class='nav-title'>BOUTIQUE</h3>
        </div>
        <ul class='nav-list'>
            <li id='products'><a href='<?php echo URL_SITE; ?>products'>Produits</a></li>
            <li id='orders'><a href='<?php echo URL_SITE; ?>orders'>Commandes</a></li>
            <li id='stats'><a href='<?php echo URL_SITE; ?>stats'>Statistiques</a></li>
        </ul>
        <div class='nav-head' id='help'>
            <div class='nav-icon'></div>
            <h3 class='nav-title'>AIDE</h3>
        </div>
        <ul class='nav-list'>
            <li id='credits'><a href='<?php echo URL_SITE; ?>credits'>Crédits</a></li>
        </ul>
    </nav>
    <img src='<?php echo URL_SITE; ?>myDP-public/svg/bd.svg' id='dev'/>
</header>
