<?php

    if(isset($_GET['page'])){
        $url_Index = explode("/", filter_var($_GET['page'], FILTER_SANITIZE_URL));
    }

    if(isset($url_Index[0]['users'])){

        print_r($url_Index);
        if($url_Index[0]['users']==='0'){
            $index=1;
        }

    }else{
        $index= 1;
    }

?>
<body>
    <main>
        <section>
            <table class='head-table'>
                <thead>
                    <tr>
                        <th class="th-user-id"></th>
                        <th class="th-user-name">Utilisateur</th>
                        <th class="th-user-contact">Contact</th>
                        <th class="th-user-verified">Infos</th>
                        <th class='th-user-type'>Type</th>
                    </tr>
                </thead>
            </table>
            <div class='table'>
                <table>
                    
                    <tbody>  
                        <?php
                            $admin_Manager = new AdminManager(); 
                            $users = $admin_Manager->getUserList();
                            $i=0;
                            foreach($users as $key){
                                

                                if($key['user_Type']==='1')
                                {
                                    $type = 'Particulier';
                                }
                                else if($key['user_Type']==='0')
                                {
                                    $type = 'Professionnel';
                                }
                                else
                                {
                                    $type = 'Administrateur';
                                }

                                echo 
                                    "<tr>",
                                        "<td class='user-infos' data-user='".$key['user_Id']."'>",
                                            "<article class='cell' data-user-article='".$key['user_Id']."'>",
                                                "<div class='user-id'><span>".$key['user_Id']."</span></div>",
                                                "<div class='user-name'><img src='".URL_SITE."myDP-public/svg/man.svg' class='user-icon'/><div class='user-info'><p class='user-fullname'>".$key['user_Firstname'].' '.$key['user_Lastname']."</p><span>Ref:</span></div></div>",
                                                "<div class='user-contact'>0695922573 </div>",
                                                "<div class='user-verified'><span class='true'>Vérifié</span></div>",
                                                "<div class='user-type'><span class='ind'>".$type."</span></div>",
                                                "<div class='user-more'>",
                                                        "<input type='checkbox' id='menu_checkbox".$i."' class='user-action' data-action='".$key['user_Id']."' >
                                                        <label for='menu_checkbox".$i."' class='label-checkbox'>
                                                          <div class='div-checkbox'></div>
                                                          <div class='div-checkbox'></div>
                                                          <div class='div-checkbox'></div>
                                                        </label>",
                                                "</div>",
                                            "</article>",
                                                    
                                            "<div class='user-more-list' data-user-action-list='".$key['user_Id']."'>",
                                               
                                                "<div class='user-more-list-section'>",
                                                    "<h3><img src='".URL_SITE."myDP-public/svg/user_green.svg' class='section-icon'/>UTILISATEUR</h3>",
                                                    "<p>Statut : <span class='verified-false'>Non vérifié</span></p>",
                                                    "<p>Inscription le : <span>21/09/2021</span></p>",
                                                    "<p>Email : <span>".$key['user_Email']."</span></p>",
                                                    "<a href='".URL_SITE."user/".$key['user_Id']."'>Accéder aux infos</a>",
                                                "</div>",
                                                "<div class='user-more-list-section'>",
                                                    "<h3><img src='".URL_SITE."myDP-public/svg/orders_2_green.svg' class='section-icon'/>COMMANDES : 2</h3>",
                                                "</div>",
                                                "<div class='user-more-list-section'>",
                                                    "<h3><img src='".URL_SITE."myDP-public/svg/tickets_green.svg' class='section-icon'/>TICKETS : 1</h3>",
                                                "</div>",
                                            "</div>",
                                        "</td>",
                                    "</tr>";
                                
                                    $i++;

                                    if($i===20){break;}
                            }  
                        ?> 
                    </tbody>
                </table>        
            </div>
            <div class='users-result-index'>

            </div>
        </section>
    </main>
</body>