<header>
    <a href="/bp" style='position: fixed;
                         height: 16vh;
                         width: 16vh;
                         order: 1;'>
      <img src="bp-public/svg/logo_1.svg">
    </a>
    <nav>
        <a href="boutique" class="link_market_t">Boutique
            <div id="link_market"></div>
        </a>
        <a <?php 
                      if(isset($_SESSION['session_Mail'])){
                        echo "
                              class='link_account_connected'>".$_SESSION['session_Firstname']."
                              <div id='link_account' class='user_Account'></div>";
                      }else{
                        echo "
                              class='link_account_t'>Se connecter
                              <div id='link_account'></div>";
                      }
                    ?>
        </a> <!-- LA BALISE SE FERME AVEC LE PHP CI-DESSUS -->
        <a class="link_cart_t">Mon Panier
            <div id="link_cart"></div>
        </a>
    </nav>
    <form>
        <label for="vinyl-search">Recherchez un vinyle</label>
        <input type="search" id="vinyl-search" name="vinyl-search" placeholder="rechercher par titre&#47;artiste&#47;album...">
        <button type="submit" id=test>
            <div></div>
        </button>
    </form>
    <nav id="jqnav" class="navigation">
      
      <input type="button" id="leave">
      <span class="link_category_type">
        <h4>Tous les produits </h4><span>&gt;</span>
      </span>
      <span class="link_category">
        <a href='#'>top ventes </a>
      </span>
      <span class="link_category">
        <a href='#'>nouveaut&eacute;s</a>
      </span>
      <span class="link_category">
        <a href='#'>offres limit&eacute;s</a>
      </span>
      <span class="link_category">
        <a href='#'>occasions</a>
      </span>
      <span class="link_category">
        <a href='#test'>mat&eacute;riels&#47;accessoires</a>
      </span>
      <span class="link_category_type">
        <h4>cat&eacute;gories</h4>
      </span>
      <span class="link_category">
        <a href='#'>pop&#47;rock&#47;m&eacute;tal</a>
      </span>
      <span class="link_category">
        <a href='#'>vari&eacute;t&eacute;s françaises</a>
      </span>
      <span class="link_category">
        <a href='#'>rap</a>
      </span>
      <span class="link_category">
        <a href='#'>musiques de films</a>
      </span>
      <span class="link_category">
        <a href='#'>raggae&#47;raggaeton</a>
      </span>
      <span class="link_category">
        <a href='#'>classique</a>
      </span>
      <span class="link_category">
        <a href='#'>country</a>
      </span>
      <span class="link_category">
        <a href='#'>electro</a>
      </span>
      <span class="link_category">
        <a href='#'>jazz</a>
      </span>
      <span class="link_category">
        <a href='#'>sixties-rock'n'roll</a>
      </span>
      
    </nav>
      <div class="right"></div>
</header>

<?php 

  if(empty($_SESSION['session_Mail'])){
    echo "<section id='connexion'>
            <div class='popup' id='connexion_popup'>
              <div class='popup_button'>
                <button class='popup_back'></button>
                <button class='popup_close'></button>
              </div>"; 

              include 'bp-content/views/login.php';
              include 'bp-content/views/register.php';

    echo "  </div>
          </section>";
  }

?>
<section id="connected">
  <div class="popup" id="connect_popup">
    <form method="post">
      <input type="submit" value="deconnexion" name="deconnexion">
    </form>
  </div>
</section>

