<?php
    require 'bp-config.php';

    // CHECKING IF THE TOKEN AND THE ID HAVE BEEN DETECTED IN THE SESSION
    if(isset($_SESSION['reset_token'])&&isset($_SESSION['reset_id']))
    { // TOKEN AND ID DETECTED

        // CONNEXION WITH THE USER MANAGER BY THE USER ID
        $user_Manager = new UserManager(htmlspecialchars($_SESSION['reset_id']));

        // CHECKING IF THE USER EXIST IN THE DB
        if($user_Manager -> user -> getId()!==null)
        { // HE EXIST

            // THEN GENERATING THE FORM TO CREATE A NEW PASSWORD
            echo 
            '
            <form method="post">
                <label>Nouveau mots de passe</label>
                <input type="password" name="new_Password">
                <label>Confirmer le nouveau mots de passe</label>
                <input type="password" name="confirm_Password">
                <input type="submit">
            </form>
            ';

        }
        else 
        { // HE DOESN'T EXIST IN THE DB 

            // INFORMING THE USER THAT NO ACCOUNT HAS BEEN FIND
            echo 
            '
            <strong>Erreur : Compte introuvable ❌</strong>
            ';

        }

    }

    // CHECKING IF THE NEW PASSWORD HAS BEEN INSERTED BY THE USER
    if(isset($_POST['new_Password'])&&isset($_POST['confirm_Password']))
    { // IT HAS BEEN

        // CHECKING IF THE PASSWORD MATCHES WITH THE CONFIRMED PASSWORD
        if($_POST['new_Password']===$_POST["confirm_Password"])
        { // IT DOES

            // SECURING THE CHAIN FOR THE DB
            $password = iProtectMySQL($_POST['new_Password']);

            // CHECKING IF THE PASSWORD MATCHES WITH THE REGULAR EXPRESSION
            if(safePass($password))
            { // IT DOES
                
                // ASKING THE CLASS USER MANAGER TO UPDATE THE PASSWORD ON THE DB
                $user_Manager -> newPassword(htmlspecialchars($_SESSION['reset_token']),htmlspecialchars($password));

            }
            else
            { // IT DOESN'T

                // INFORMING THE USER THAT HIS PASSWORD ISN'T COMPLETE
                echo "Le mot de passe doit au moins contenir 8 charactères, dont 1 majuscule, 1 minuscule, 1 chiffre, et 1 caractère spéciale minimum.";

            }

        }
        else
        {

            // INFORMING THE USER THAT BOTH PASSWORDS HAVE TO BE IDENTICAL
            echo "Attention, les deux mots de passe doient être identique";

        }

    }
    else
    { // IT HASN'T BEEN

        // INFORMING THE USER THAT HE HAS TO COMPLETE FIEDS.
        echo 'Veuillez remplir les champs';

    }

?>
