<?php 

    // Ce fichier est une bibliothèque de fonction permettants de sécuriser le site.

    function thisPassIsSecured($password){

        $password = trim($password);

        $reg = "#^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[~!@\#$%^&*()\-_=+\[\]{};:,\.<>/?|])[A-Za-z\d~!@\#$%^&*()\-_=+\[\]{};:,\.<>/?|]{8,}$#";

        // On vérifie que le mots de passe respecte bien l'expression régulière
        if(preg_match($reg,$password)){
            
            $password = iProtectMySQL($password);

            $password = password_hash($password,PASSWORD_BCRYPT);

            return $password;

        } else {

            return false;

        }

    }


    function iProtectMySQL($var){

        $var = strip_tags($var);

        $var = str_replace('*','2Z',$var);
        $var = str_replace('(','4J',$var);
        $var = str_replace(')','M0',$var);
        // $var = str_replace('.','B7',$var);
        $var = str_replace('`','V3',$var);
        $var = str_replace('#','S9',$var);
        $var = str_replace('<script>','N5',$var);
        $var = str_replace('</script>','X1',$var);
        $var = str_ireplace('DROP','Q3K',$var);
        $var = str_ireplace('INSERT','Z9D',$var);
        $var = str_ireplace('UPDATE','SD8',$var);
        $var = str_ireplace('SELECT','F2G',$var);
        $var = str_ireplace('WHERE','ZIE',$var);
        $var = str_ireplace('FROM','KS0',$var);
        $var = str_ireplace('DELETE','SD',$var);

        $var = htmlspecialchars($var);

        return $var;

    }

    function safeEmail($var){
        $var = strip_tags($var);
        $var = htmlspecialchars($var);
        $var = str_ireplace('DROP','SQL.INJ.DROP',$var);
        $var = str_ireplace('INSERT','SQL.INJ.INS',$var);
        $var = str_ireplace('UPDATE','SQL.INJ.UPD',$var);
        $var = str_ireplace('SELECT','SQL.INJ.SEL',$var);
        $var = str_ireplace('WHERE','SQL.INJ.WHE',$var);
        $var = str_ireplace('FROM','SQL.INJ.FROM',$var);
        $var = str_ireplace('DELETE','SQL.INJ.DEL',$var);

        return $var;
    }

    function safePass($var){
        
        $reg = "#^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[~!@\#$%^&*()\-_=+\[\]{};:,\.<>/?|])[A-Za-z\d~!@\#$%^&*()\-_=+\[\]{};:,\.<>/?|]{8,}$#";

        if(preg_match($reg,$var))
        {
            return true;
        }else{
            return false;
        }

    }

    function unknowForMoreSecurity($length = 10)
    {
        $chain = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $maxLength = strlen($chain);
        $unknownChain = '';
        for ($i = 0; $i < $length; $i++){
            $unknownChain .= $chain[rand(0, $maxLength - 1)];
        }
        return $unknownChain;
    }

    

    // function phoneVerification($phone){
    //     $regex="#^[0]+[6-7]+([-. ]?[0-9]{2}){4}$#";
    //     if(preg_match($regex,$phone)){
    //         return true;
    //     }else{
    //         return false;
    //     }
    // }

    // function zipVerification($zip){
    //     $tab=str_split($zip);
    //     $longueur=count($tab);
    //     if($longueur===5){
    //         return true;
    //     }else{
    //         return false;
    //     }
    // }
    
    // function nameVerification($var){
    //     $regex="#^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$#";
    //     if(preg_match($regex,$var)){
    //         return true;
    //     }else{
    //         return false;
    //     }
    // }

    // function adresseVerification($var){
    //     $regex="#^[0-9a-zA-Z ]{1,}$#";
    //     if(preg_match($regex,$var)){
    //         return true;
    //     }else{
    //         return false;
    //     }
    // }
    

?>