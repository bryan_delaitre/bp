<?php

    class User extends Model {
        private $user_Id;
        private $user_Email;
        private $user_Fullname;
        private $user_Firstname;
        private $user_Lastname;
        private $user_Password;
        private $user_Type;

        public function __construct($id=null, $email=null, $userFirstname=null, $userLastname=null, $password=null, $type=null){
            
            $this -> user_Id = $id;
            $this -> user_Email = $email;
            $this -> user_Fullname = strtoupper($userLastname)." ".ucfirst($userFirstname);
            $this -> user_Firstname = $userFirstname; 
            $this -> user_Lastname = $userLastname;
            $this -> user_Password = $password;
            $this -> user_Type = $type;

        }

        public function getId(){
            return $this -> user_Id;
        }

        public function setId($id){
            $this -> user_Id = $id;
        }

        public function getEmail(){
            return $this -> user_Email;
        }

        public function setEmail($email){
            $this -> user_Email = $email;
        }

        public function getFullname(){
            return $this -> user_Fullname;
        }

        public function setFullname($fullname){
            $this -> user_Fullname = $fullname;
        }

        public function getFirstname(){
            return $this -> user_Firstname;
        }

        public function setFirstname($firstname){
            $this -> $user_Firstname = $firstname;
        }

        public function getLastname(){
            return $this -> $user_Lastname;
        }

        public function setLastname($lastname){
            $this -> user_Lastname = $lastname;
        }

        private static function getPassword(){
            return $this -> user_Password;
        }

        private static function setPassword($password){
            $this -> user_Password = $password;
        }
       
    }




?>