<?php

    require_once "Model.php";
    require_once "User.php";
    require 'bp-content/library/safetyFirst.php';

    class UserManager extends Model 
    {

        public $user;
        private $email_Session;

        public function __construct($id=null)
        {   
            // CHECKING THAT THE ID IS NOT EMPTY
            if(isset($id))
            {

               
                // SECURING THE CHAIN
                $id = iProtectMySQL($id);

                // CHECKING IF THE ID IS AN EMAIL
                if(filter_var($id, FILTER_VALIDATE_EMAIL))
                {

                    // SAVING THE EMAIL AS A VARIABLE 
                    $this -> email_Session = $id;

                    // RECOVERING THE USER INFORMATIONS BY USING HIS EMAIL
                    $user = $this -> getDatabase() -> select('users','*',[
                        'user_Email' => $this -> email_Session
                    ]);

                    // SAVING THE USER INFORMATION AS A CLASS
                    if(!empty($user))
                    {
                        $this -> user = new User($user[0]['user_Id'],$user[0]['user_Email'],$user[0]['user_Firstname'],$user[0]['user_Lastname'],$user[0]['user_Password'],$user[0]['user_Type']);
                    } 

                }
                // ELSE -> HAPPEN IF ID IS NOT AN EMAIL 
                else 
                {

                    // RECOVERING THE USER INFORMATIONS BY USING HIS ID
                    $user = $this -> getDatabase() -> select('users','*',[
                        'user_Id' => $id
                    ]);

                    // SAVING THE USER INFORMATION AS A CLASS
                    if(!empty($user))
                    {
                        $this -> user = new User($user[0]['user_Id'],$user[0]['user_Email'],$user[0]['user_Firstname'],$user[0]['user_Lastname'],$user[0]['user_Password'],$user[0]['user_Type']);
                    } 

                }

            }
            
        }

        public function getUserById($id)
        {
            if(isset($id)&&$id!==null){
                $user = $this -> getDatabase() -> select('users','*',[
                    'user_Id' => $id
                ]);

                if(!empty($user))
                {
                    $this -> user = new User($user[0]['user_Id'],$user[0]['user_Email'],$user[0]['user_Firstname'],$user[0]['user_Lastname'],$user[0]['user_Password'],$user[0]['user_Type']);
                }
                else
                {
                    return "Utilisateur introuvable ❌";
                }

            }
        }

        public function getUser()
        {
            return $this -> user;
        }

        public function connexionUser($Email, $Password, $Ip)
        {

            $this -> getDatabase() -> insert('users_connexions_logs',[
                'connexion_Login' => $Email,
                'connexion_Ip' => $Ip
            ]);

            $connexion=$this -> getDatabase() -> select('users','*',[
                'user_Email' => $Email
            ]);
            
            $hashed_Password=$connexion[0]['user_Password'];

            if(password_verify($Password,$hashed_Password))
            {

                $_SESSION['session_Mail']=$Email;
                $_SESSION['session_Firstname']=$connexion[0]['user_Firstname'];
                $_SESSION['session_Lastname']=$connexion[0]['user_Lastname'];
                $_SESSION['session_Password']=$hashed_Password;
                $_SESSION['session_Type']=$connexion[0]['user_Type'];
                $_SESSION['session_Admin']=$connexion[0]['user_Is_Admin'];

                if($connexion[0]['user_Is_Admin']==='1')
                {
                    header("Location:eAdmin");
                }

                else
                {
                    header("Location:/bp");
                }
                
            }
            else
            {
                // header('Location:/lolilol');
            }
            

        }

        public function deconnexionUser()
        {

            session_destroy();
            header("Location:/bp");

        }

        // FUNCTION THA GIVE THE POSSIBILITY TO THE USER TO CREATE AN ACCOUNT
        public function insertNewUser($LastName,$FirstName,$Email,$Password,$Type)
        {

            // CHECKING THE EXISTANCE OF A POSSIBLE ACCOUNT
            if(empty($this -> user)){ // NO ACCOUNT DETECTED

                $Password = password_hash($Password,PASSWORD_BCRYPT);   

                // CREATING THEN AND INSERTING THE NEW ACCOUNT IN THE DB
                $this -> getDatabase() -> insert('Users',[
                    'user_Lastname' => strtoupper($LastName),
                    'user_Firstname' => ucfirst($FirstName),
                    'user_Email' => strtolower($Email),
                    'user_Password' => $Password,
                    'user_Type' => $Type,
                ]);

                $user_Id = $this -> getDatabase() -> select('users',[
                    'user_Id'
                ],[
                    'user_Email' => $Email
                ])[0]['user_Id'];

                $this -> user = new User($user_Id,$Email,$FirstName,$LastName,$Password);

                $log_Message = $FirstName.' '.$LastName.' vient de créer un compte';

                $log_Report = '✔️ Succès';

                $_SESSION['session_Mail'] = $Email;
                $_SESSION['session_Firstname'] = "$FirstName";
                $_SESSION['session_Lastname'] = "lol";
                $_SESSION['session_Type'] = $Type;
                $_SESSION['session_Password'] = $Password;

            } else { // A ACCOUNT HAS BEEN DETECTED

                // WRITING A LOG TO INFORM THE ADMIN THAT SOMEONE TRY TO CREATE AN ACCOUNT WITH AN ALREADY USED EMAIL
                $log_Message = $FirstName.' '.$LastName.' à tenter de créer un compte';

                $log_Report = '❌ Erreur, un compte existe déjà à cette adresse email "'.$Email.'"';

                echo "<span data-error>".$log_Report."</span>";

            }


            $user_Id = $this -> getDatabase() -> select('users',[
                'user_Id'
            ],[
                'user_Email' => $Email
            ])[0]['user_Id'];

            // INSERTING THE LOG
            $this -> getDatabase() -> insert('users_logs',[
                'log_Event' => $log_Message,
                'log_Event_Type' => 'ACC.NEW',
                'log_Event_Report' => $log_Report,
                'user_Id' => $user_Id
            ]);

        }

        
        // FUNCTION THA GIVE THE POSSIBILITY TO THE USER TO UPDATE HIS FIRSTNAME
        public function updateFirstName($FirstName)
        {

            // On vérifie l'éxistance ou non d'un compte avec cette adresse email
            if(empty($this -> user)){

                $log_Message = "L'utilisateur avec l'email ".$this -> email_Session    ." à essayer de mettre à jour son prénom";

                $log_Report = "❌ Erreur, Impossible de trouver un compte avec cette adresse email : ".$this -> email_Session;

                echo "<span data-error>".$log_Report."</span>";

                $this -> getDatabase() -> insert('users_logs',[
                    'log_Event' => $log_Message,
                    'log_Event_Type' => 'ACC.MAJ',
                    'log_Event_Report' => $log_Report,
                    'user_Id' => "?"
                ]);

            }

            else if(strlen($FirstName)>=2 && !empty($this -> user)){
               
                $this -> getDatabase() -> update('users',[
                    'user_Fistname' => $FirstName
                ],[
                    'user_Id' => $this -> user -> getId()
                ]);

                $log_Message = "Le client N°".$this -> user -> getId()." a mis à jour son prénom";

                $log_Report = "✔️ Succès";

                $this -> getDatabase() -> insert('users_logs',[
                    'log_Event' => $log_Message,
                    'log_Event_Type' => 'ACC.MAJ',
                    'log_Event_Report' => $log_Report,
                    'user_Id' => $this -> user -> getId()
                ]);

            } else {

                $log_Message = "Le client N°".$this -> user -> getId()." à essayer de mettre à jour son prénom";

                $log_Report = "❌ Erreur, le nouveau prénom fait moins de 2 caractères.";
            
                echo "<span data-error>".$log_Report."</span>";

                $this -> getDatabase() -> insert('users_logs',[
                    'log_Event' => $log_Message,
                    'log_Event_Type' => 'ACC.MAJ',
                    'log_Event_Report' => $log_Report,
                    'user_Id' => $this -> user -> getId()
                ]);

            }

            

        }

        // FUNCTION THA GIVE THE POSSIBILITY TO THE USER TO UPDATE HIS PASSWORD BY HIS FRONT OFFICE
        public function updatePassword($password)
        {

            if(empty($this -> user)){

                $log_Message = "L'utilisateur avec l'email ".$this -> email_Session    ." à essayer de mettre à jour son mot de passe";

                $log_Report = "❌ Erreur, Impossible de trouver un compte avec cette adresse email : ".$this -> email_Session;

                echo "<span data-error>".$log_Report."</span>";

                $this -> getDatabase() -> insert('users_logs',[
                    'log_Event' => $log_Message,
                    'log_Event_Type' => 'ACC.MAJ',
                    'log_Event_Report' => $log_Report,
                    'user_Id' => "?"
                ]);

            } else if(!empty($this -> user)){

                if(thisPassIsSecured($password)){

                    $password = thisPassIsSecured($password);

                    $this -> getDatabase() -> update('users',[
                        'user_Password' => $password
                    ],[
                        'user_Id' => $this -> user -> getId()
                    ]);

                    $log_Message = "L'utilisateur avec l'email ".$this -> email_Session    ." à mis à jour son mot de passe";

                    $log_Report = "✔️ Succès";
    
                    $this -> getDatabase() -> insert('users_logs',[
                        'log_Event' => $log_Message,
                        'log_Event_Type' => 'ACC.MAJ',
                        'log_Event_Report' => $log_Report,
                        'user_Id' => $this -> user -> getId()
                    ]);

                } else {

                    $log_Message = "L'utilisateur avec l'email ".$this -> email_Session    ." à essayer de mettre à jour son mot de passe";

                    $log_Report = "❌ Erreur, le mot de passe n'est pas assez sécurisé";
    
                    echo "<span data-error>".$log_Report."</span>";
        
                    $this -> getDatabase() -> insert('users_logs',[
                        'log_Event' => $log_Message,
                        'log_Event_Type' => 'ACC.MAJ',
                        'log_Event_Report' => $log_Report,
                        'user_Id' => $this -> user -> getId()
                    ]);

                }

            }

        }

        // FUNCTION THAT GIVE THE POSSIBILITY TO THE USER TO DELETE HIS ACCOUNT
        public function deleteUser()
        {

            if(!empty($this -> user)){

                $id = $this -> user -> getId();

                $Fullname = $this -> user -> getFullname();

                $this -> getDatabase() -> delete('users',[
                    'user_Id' => $this -> user -> getId()
                ]);

                $log_Message = $Fullname.' vient de supprimer son compte';

                $log_Report = "✔️ Succès";
    
                $this -> getDatabase() -> insert('users_logs',[
                    'log_Event' => $log_Message,
                    'log_Event_Type' => 'ACC.DEL',
                    'log_Event_Report' => $log_Report,
                    'user_Id' => $id
                ]);

            }

        }

        // FUNCTION THAT GIVE THE POSSIBILITY TO THE USER TO ASK FOR A RESET OFF HIS PASSWORD
        public function resetPassword($Email)
        {

            // WE RECOVER THE INFORMATIONS OF THE EXISTING USER
            $user=$this -> getDatabase() -> select('users','*',[
                'user_Email' => $Email
            ])[0];

            if(isset($user['user_Email']))
            {

                // WE CANCEL THE ACTIVE TOKEN (if existing)
                $this -> getDatabase() ->update('users_pass_reset',[
                    'Pass_Reset_Expired' => 1
                ],[
                    'Pass_Reset_Expired' => 0,
                    'user_Id' => $user['user_Id']
                ]);

                // CREATION OF THE TOKEN (secured but heavy to generate guess why ^^)
                $hashs=hash_algos();
                $random_Hash=array_rand(hash_algos(), 1);
                $token=hash($hashs[$random_Hash],unknowForMoreSecurity(10));
                do{
                    $random_Hash=array_rand(hash_algos(), 1);
                    $token.=hash($hashs[$random_Hash],unknowForMoreSecurity(10));
                }while((strlen($token))<50);
                $token=substr($token,0,50);

                $this -> getDatabase() -> insert('users_pass_reset',[
                    'user_Id' => $user['user_Id'],
                    'pass_reset_token' => $token
                ]); 

                // CREATION OF THE LINK (needed for the password reset)
                $link_to_reset="http://localhost/bp/reinitialision-mot-de-passe/id/".$user['user_Id']."/".$token;

                // BODY OF THE SENT EMAIL
                $body="bonjour ".$user['user_Firstname']." voici votre lien pour réinitialiser votre mots de passe ✅: ".$link_to_reset."";

                // SENDING THE TOKEN BY EMAIL
                send_mail("brdelaitre@gmail.com","🔃 Réinitialisation de votre mots de passe",$body);

                // INFORMING THE USER OF THE TOKEN SENT ON HIS EMAIL
                echo "<strong>Un email vient de vous être envoyé afin de réinitialiser votre mots de passe ✅</strong>";

                // UPDATING ADMIN LOGS
                $this -> getDatabase() -> insert('users_logs',[
                    'log_Event' => $this -> user -> getFullname()." souhaite réinitialiser son mot de passe",
                    'log_Event_Type' => 'ACC.LOST',
                    'log_Event_Report' => "✔️ Token envoyé",
                    'user_Id' => $this -> user -> getId()
                ]);

            }

        }

        // ^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[~!@#$%^&*()\-_=+\[\]{};:,\.<>/?|])[A-Za-z\d~!@#$%^&*()\-_=+\[\]{};:,\.<>/?|]{8,}$

        public function newPassword($token, $password)
        {

            // CHEKING IF THE TOKEN EXIST OR IF IT'S STILL USABLE
            $Token_Existing= $this -> getDatabase() -> select('users_pass_reset','*',[
                'Pass_Reset_Token' => $_SESSION['reset_token'],
                'user_Id' => $_SESSION['reset_id']
            ])[0];

            if(isset($Token_Existing['Pass_Reset_Id']))
            { // IT EXIST

                // AND IT IS STILL AVAILABLE
                if($Token_Existing['Pass_Reset_Expired']==="0")
                {

                    // USING A FUNCTION THAT CHECK IF THE PASSWORD MATCHES WITH THE REGEX
                    if(thisPassIsSecured($password))
                    { // IT MATCHES
                        
                        // UPDATING THE DB WITH THE NEW PASSWORD
                        $this -> getDatabase() -> update ('users',[
                            'user_Password' => thisPassIsSecured($password)
                        ],[
                            'user_Id' => $this -> user -> getId()
                        ]);

                        // UPDATING THE DB TO MAKE THE TOKEN INVALID
                        $this -> getDatabase() -> update ('users_pass_reset',[
                            'Pass_Reset_Expired' => '1'
                        ],[
                            'Pass_Reset_Token' => iProtectMySQL($token)
                        ]);

                        echo 'Le mots de passe a bien été mis à jour ✔️';

                        $log_Message = $this -> user -> getFullname().' vient de réinitialiser son mot de passe';

                        $log_Report = "✔️ Succès";

                    }
                    
                }
                else
                { // THE TOKEN HAS BEEN ALREADY USED OR IS UNAVAILABLE

                    echo "Le Token n'éxiste pas ou n'est plus valide ❌";
                    
                    $log_Message = $this -> user -> getFullname()." a essayé de réinitialiser son mot de passe";

                    $log_Report = "❌ Erreur, l'utilisateur a tenté d'utiliser un token invalide pour réinitialiser son mot de passe";

                }

            }
            else
            { // THE TOKEN DOESN'T EXIST

                echo "Le Token n'éxiste pas ou n'est plus valide ❌";

                $log_Message = $this -> user -> getFullname()." a essayé de réinitialiser son mot de passe";

                $log_Report = "❌ Erreur, l'utilisateur a tenté d'utiliser un token inéxistant pour réinitialiser son mot de passe";

            }

            $this -> getDatabase() -> insert('users_logs',[
                'log_Event' => $log_Message,
                'log_Event_Type' => 'ACC.LOST',
                'log_Event_Report' => $log_Report,
                'user_Id' => $this -> user -> getId()
            ]);

        }


    }

?>