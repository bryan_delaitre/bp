<?php

        if(isset($_POST['connexion'])&&$_POST['connexion']==='confirmed')
        {  
                    $user_Connect_Mail = htmlspecialchars(safeEmail($_POST["login_Email"]));
                    $user_Connect_Pass = iProtectMySQL($_POST["login_Pass"]);
                    $user_Connect_Ip = password_hash($_SERVER['REMOTE_ADDR'],PASSWORD_BCRYPT);

                    if(filter_var($user_Connect_Mail, FILTER_VALIDATE_EMAIL)){

                        $user_Manager = new UserManager($user_Connect_Mail);

                        $user_Manager -> connexionUser($user_Connect_Mail,$user_Connect_Pass,$user_Connect_Ip);

                    }      
        }
        
?>