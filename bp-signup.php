<?php

if(isset($_POST['register_Submit'])&&$_POST['register_Submit']==='confirme'){

    $register_Email = safeEmail($_POST['register_Email']);

    if(filter_var($register_Email, FILTER_VALIDATE_EMAIL)){
        
        $user_Manager = new UserManager($register_Email);
        
        $register_Firstname = iProtectMySQL($_POST['register_Firstname']);
        $register_Lastname = iProtectMySQL($_POST['register_Lastname']);
        $register_Password = iProtectMySQL($_POST['register_Password_Confirm']);
        $register_Type = iProtectMySQL($_POST['register_Individual']);

        if($register_Type!=='0'||$register_Type!=='1'){
            $register_Type='1';
        }

        $user_Manager -> insertNewUser($register_Lastname,$register_Firstname,$register_Email,$register_Password,$register_Type);

    }

}

?>